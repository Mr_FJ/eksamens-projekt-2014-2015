﻿using UnityEngine;
using System.Collections;

public class FocusMode : MonoBehaviour {

	// Use this for initialization
	void Start () {
        DebugGUI.Instance.Log("auto focus");
        bool focusModeSet = CameraDevice.Instance.SetFocusMode(
        CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

        if (!focusModeSet)
        {
            DebugGUI.Instance.Log("Failed to set focus mode (unsupported mode).");
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            DebugGUI.Instance.Log("Focus tap");
            SetFocus();
        }
	}
    private void SetFocus()
    {
        
        bool focusModeSet = CameraDevice.Instance.SetFocusMode(
        CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);

        if (!focusModeSet)
        {
            DebugGUI.Instance.Log("Failed to set focus mode (unsupported mode).");
        }
    }
}
