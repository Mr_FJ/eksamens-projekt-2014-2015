﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugGUI : MonoBehaviour {

    private static DebugGUI _instance;
    public int AmountShownLogs=1;
    private List<string> _log;
	// Use this for initialization
    public static DebugGUI Instance {
        get 
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<DebugGUI>();
            return _instance;
        }
    }
    public void Awake()
    {
        _log = new List<string>();
    }
    public void OnGUI()
    {
        int startindex = _log.Count - AmountShownLogs;
        if(startindex<0)
        {
            startindex = 0;
        }
        GUILayout.BeginArea(new Rect(5, 5, Screen.width - 10, Screen.height-10));
        for (int i = startindex; i < _log.Count; i++)
        {
            GUILayout.Label(_log[i]);
        }
        GUILayout.EndArea();
    }
    public void Log(string text)
    {
        Debug.Log(_log.Count);
        Debug.Log(text);
        _log.Add(text);
    }
    public void Clear()
    {
        _log = new List<string>();
    }
}
